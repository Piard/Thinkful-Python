__author__ = 'hpiard'
import sys

def test_division(num1, num2):
    if (num1 % num2 == 0):
        return True
    else:
        return False

def get_value():
    if sys.argv[1:]:
        userinput = sys.argv[1]
    else:
        userinput = raw_input("Enter the upper limit of the range: ")
    return userinput

def convert_value(userinput):
    while True:
        try:
            value = int(userinput)
            return value
        except ValueError:
            userinput = raw_input("Incorrect data type. Please enter an 'integer' for the upper limit of the range: ")

def fizzbuzzrefactor(userinput=100):
    print "This is my fizzbuzz programm!"
    #print userinput
    print "Fizzbuzz counting up to {}".format(userinput)
    fizzbuzz = "fizz buzz"
    fizz = "fizz"
    buzz = "buzz"
    for i in xrange(1, userinput + 1):
        if test_division(i, 15):
            print fizzbuzz
        elif test_division(i, 5):
            print fizz
        elif test_division(i, 3):
            print buzz
        else:
            print i

if __name__ == '__main__':
    counter = get_value()
    #print type(counter)
    counter2 = convert_value(counter)
    fizzbuzzrefactor(counter2)