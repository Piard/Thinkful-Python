__author__ = 'hpiard'
import sys

print "This is my fizzbuzz program."
#userinput = int(raw_input('Please enter a number: '))
if sys.argv[1:]:
    userinput = sys.argv[1]
else:
    userinput = raw_input("Enter the upper limit of the range: ")

print userinput
print "Fizzbuzz counting up to {}".format(userinput)
fizzbuzz = "fizz buzz"
fizz = "fizz"
buzz = "buzz"

for i in xrange(1, int(userinput) + 1):
    if (i % 3 == 0) and (i % 5 == 0):
        print fizzbuzz
    elif (i % 3 == 0) and (i % 5 != 0):
        print fizz
    elif (i % 3 != 0) and (i % 5 == 0):
        print buzz
    else:
        print i