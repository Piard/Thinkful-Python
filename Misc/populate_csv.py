__author__ = 'hpiard'
import sys
import os
import csv
import datetime
import sqlite3


def populate2():
    with open('show_history.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            print(' ,'.join(row))


def populate():
    dbconnect = sqlite3.connect('../db.sqlite3')
    c = dbconnect.cursor()
    y = 0
    with open('show_history.csv') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            formatted_date_time = datetime.datetime.strptime(row['Time stamp'], '%H:%M %d/%m/%y')\
                .strftime('%Y-%m-%d %H:%M:%M')
            y += 1
            data_to_import = (str(y), formatted_date_time, row['Description'], str(y))
            #print(data_to_import)
            c.execute('INSERT INTO test_table(id, date_time, tick_volume, id2) VALUES  (?, ?, ?, ?)', data_to_import)
    dbconnect.commit()
    c.close()


if __name__ == "__main__":
    print("Starting population into SQLite DB rxall.....\n")
    populate()